import cv2
import threading
from queue import Queue
import time

# Function to continuously capture frames
def capture_frames(cap, frame_queue):
    

    while True:
        ret, frame = cap.read()
        if ret:
            frame_queue.put(frame)

# Function to save frames as image files
def save_frames(frame_queue):
    count = 0
    while True:
        if not frame_queue.empty():
            frame = frame_queue.get()
            cv2.imwrite(f"image.jpg", frame)
            count += 1
            time.sleep(0.01)  # Introduce a 50ms delay

# Open the webcam
cap = cv2.VideoCapture(1)

# Create a queue to store frames
frame_queue = Queue(maxsize=100)

# Create threads for capturing and saving frames
capture_thread = threading.Thread(target=capture_frames, args=(cap, frame_queue))
save_thread = threading.Thread(target=save_frames, args=(frame_queue,))

# Start the threads
capture_thread.start()
save_thread.start()

# Wait for the threads to finish (you can set a condition to stop the loop)
while True:
    if not capture_thread.is_alive() and frame_queue.empty():
        break

# Release the webcam
cap.release()
