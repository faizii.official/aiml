import cv2

# Load the required trained XML classifiers
face_cascade = cv2.CascadeClassifier('data/face.xml')

# Capture frames from a camera
cap = cv2.VideoCapture(1)

# Initialize face counter
face_counter = 0

# Loop runs if capturing has been initialized.
while True:
    # Read frames from a camera
    ret, img = cap.read()

    # Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Detect faces of different sizes in the input image
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    # For each detected face, draw a rectangle and label it
    for (x, y, w, h) in faces:
        # Draw a rectangle around the face
        cv2.rectangle(img, (x, y), (x+w, y+h), (255, 255, 0), 2)

        # Increment face counter
        face_counter += 1

        # Add face label
        label = f'Face {face_counter}'
        cv2.putText(img, label, (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (255, 255, 0), 2)

    # Display the image
    cv2.imshow('img', img)

    # Wait for Esc key to stop
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

# Close the window
cap.release()

# De-allocate any associated memory usage
cv2.destroyAllWindows()
