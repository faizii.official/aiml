import numpy as np
import tensorflow as tf

# Load the TFLite model and allocate tensors
interpreter = tf.lite.Interpreter(model_path="model.tflite")
interpreter.allocate_tensors()

# Get input and output details
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Load and preprocess the input data
input_data = np.random.randint(0, 255, size=input_details[0]['shape'], dtype=np.uint8)  # Generate random UINT8 data
interpreter.set_tensor(input_details[0]['index'], input_data)

# Run inference
interpreter.invoke()

# Get the output tensor data
output_data = interpreter.get_tensor(output_details[0]['index'])

# Analyze the output data
print("Output shape:", output_data.shape)
print("Output data:", output_data)
